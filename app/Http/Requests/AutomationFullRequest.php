<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AutomationFullRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'nullable|string',
            'active' => 'required|boolean',
                //validamos el grupo_events como hijo
                'group_events' => 'nullable|array',
                'group_events.*.name' => 'required|string',
                'group_events.*.description' => 'nullable|string',
                    //comprobamos los eventos
                    'group_events.*.events' => 'nullable|array',
                    'group_events.*.events.*.*.type_code' => 'required|integer', 
                    'group_events.*.events.*.*.email_dispatch' => 'nullable|string',
                    'group_events.*.events.*.*.click_dispatch' => 'nullable|string',
                //validamos el grupo_actions como hijo
                'group_actions' => 'nullable|array',
                'group_actions.*.name' => 'required|string',
                'group_actions.*.description' => 'nullable|string',
                    //comprobamos las acciones
                    'group_actions.*.actions' => 'nullable|array',
                    'group_actions.*.actions.*.*.type_code' => 'required|integer', 
                    'group_actions.*.actions.*.*.sender_email' => 'nullable|string',
                    'group_actions.*.actions.*.*.tag' => 'nullable|string',

        ];
    }
}
