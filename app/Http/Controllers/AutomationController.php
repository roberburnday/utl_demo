<?php

namespace App\Http\Controllers;

use App\Http\Requests\AutomationFullRequest;
use App\Http\Requests\AutomationRequest;
use App\Http\Requests\EventRequest;
use App\Models\Automation;
use App\Models\Event;
use App\Models\GroupEvents;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class AutomationController extends Controller
{
    public function getList(Request $request){
        $automations = Automation::all();

        return response()->json($automations);
    }

    public function getAutomation(Automation $automation){
        //with -- antes de cargar el get/find/all/lo que sea
        //load -- carga relaciones despues del get/find/all/lo que sea

        $automation = $automation->load('group_actions.actions','group_events.events');

        return response()->json($automation);
    }

    public function createAutomation(AutomationRequest $request){
        try{
            DB::beginTransaction();            
            $automation = new Automation();
            $automation->fill($request->all());
            $automation->save();
            DB::commit();
            return response()->json($automation);
        }catch(Exception $e){
            DB::rollBack();
            return response()->json($e, 500);
        }
    }

    public function updateAutomation(AutomationRequest $request, Automation $automation){
        $automation->fill($request->all());
        $automation->save();
        return response()->json($automation);
    }

    public function createFullAutomation(AutomationFullRequest $request){
        try{
            DB::beginTransaction();
            $automation = new Automation();
            $automation->fill($request->all());
            $automation->save();
            //crear grupo de eventos y de acciones
            $group_evt = $automation->group_events->create($request->group_events);
            $group_act = $automation->group_actions->create($request->group_actions);
            //create batch 
            $group_evt->events->insert($request->group_events->events);
            $group_act->actions->insert($request->group_actions->actions);
            DB::commit();
            return response()->json($automation);
        }catch(Exception $e){
            DB::rollBack();
            return response()->json($e, 500);
        }
    }

    public function updateFullAutomation(AutomationFullRequest $request, Automation $automation){
        try{
            DB::beginTransaction();
            $automation->fill($request->all());
            $automation->save();
            //editar grupo de eventos y de acciones
            $group_evt = $automation->group_events()->update($request->group_events);
            $group_act = $automation->group_actions()->update($request->group_actions);
            //update batch --> tendría que darle una vuelta a este punto
            //en principio es una demo --> si se requiere una respuesta concreta sobre este punto _*
            // _* dadme un día y se estudia

            //consejo: yo haría los updates mediante metodos separados, pero esto es para ceñirme al _*
            // _* codigo existente
            $group_evt->events->upsert($request->group_events->events, [
                'id'
            ], [
                'type_code', 'email_dispatch', 'click_dispatch'
            ]);
            $group_act->actions->upsert($request->group_actions->actions, [
                'id'
            ], [
                'type_code', 'sender_email', 'tag'
            ]);
            DB::commit();
            return response()->json($automation);
        }catch(Exception $e){
            DB::rollBack();
            return response()->json($e, 500);
        }
    }

    /**
     * Solo hay uno de muestra. Es una demo
     */
    public function addEvent(GroupEvents $group_evt, EventRequest $request){
        $event = $group_evt->events->create($request);

        return response()->json($event);
    }

    public function editEvent(Event $event, EventRequest $request){
        $event->fill($request->all())->save();

        return response()->json($event);
    }
}
