<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        //no hace falta lanzar ningún systemctl --> esto se ejecuta cuando le dices y puedes pararlo y arrancarlo en local
        //no requiere instalación
        //no requiere comprobar el estado --> lo activas o lo das de baja para el test --> no hay que ponerlo en el arranque
        //no hay que modificar nada en el ordenador para que funcione (funciona con modificación 0 del sistema, del entorno y 
        // con indepencia del sistma operativo)
        $schedule->command('rabbit:read')->everyTwoMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
