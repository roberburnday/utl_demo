<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ReadRabbitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbit:read';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read Rabbit MQ logs';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //aquí se pondría la lectura del RabbitMQ o cualquier acción automatica

        //Nos beneficiamos de que la logica se puede extraer en un service, permitiendo que estas acciones sean llamadas desde un
        //controlador o desde un commando.

        //Contamos con el excelente tratamiento de los modelos de laravel, que optimizan y reducen las lineas de código

        //Es una demo, no voy a configurar rabbit, pero recuerdo que se puede instalar y configurar en poco tiempo:
        //https://github.com/vyuldashev/laravel-queue-rabbitmq

        return Command::SUCCESS;
    }
}
