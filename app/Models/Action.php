<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    use HasFactory;

    //es una demo, no contiene todos los campos
    protected $fillable = ['type_code', 'sender_email', 'tag'];

    public function group_actions(){
        $this->belongsTo(GroupActions::class);
    }

}
