<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupActions extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description'];

    public function automation(){
        return $this->belongsTo(Automation::class);
    }

    public function actions(){ 
        return $this->hasMany(Action::class);
    }
}
