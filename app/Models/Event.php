<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    //es una demo, no contiene todos los campos
    protected $fillable = ['type_code', 'email_dispatch', 'click_dispatch'];

    public function group_events(){
        $this->belongsTo(GroupEvents::class);
    }

}
