<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupEvents extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description'];

    public function automation(){
        return $this->belongsTo(Automation::class);
    }

    public function events(){
        return $this->hasMany(Event::class);
    }
}
