<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Automation extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'active'];

    public function group_actions(){
        return $this->hasOne(GroupActions::class);
    }

    public function group_events(){
        return $this->hasOne(GroupEvents::class);
    }

    public function scopeActived($query)
    {
        return $query->where('active', 1);
    }
}
