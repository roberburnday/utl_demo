<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\AutomationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post('/auth/register', [AuthController::class, 'createUser']);
Route::post('/auth/login', [AuthController::class, 'loginUser']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'AUT', 'middleware' => 'auth:sanctum'], function () {
    Route::get('/list', [AutomationController::class,'getList']);
    Route::get('/{automation}', [AutomationController::class,'getAutomation']);

});
